function spice([input]) {
    let resources = Number(input);
    let days = 0;
    let spices = 0;

    while (resources >= 100) {
        days++;
        spices += resources - 26;
        resources -= 10;
    }

    if (spices > 26) {
        spices -= 26;
    } else {
        spices = 0;
    }

    console.log(days);
    console.log(spices);
}

spice([111])
//spice(450)
//spice(200)