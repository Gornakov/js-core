function solve(input) {
    let availablePlanes = {};
    let sortAvailablePlanes = [];
    let towns = {};
    let dataArr = new Array();

    for (let line of input) {
        let data = line.split(' ');
        let plane = data[0];
        let town = data[1];
        let people = Number(data[2]);
        let action = data[3];

        if (action === 'land') {
            if (availablePlanes[plane] === undefined) {
                availablePlanes[plane] = plane;

                if (towns[town] === undefined) {
                    towns[town] = {};
                    towns[town].townName = town;
                    towns[town].arrivals = people;
                    towns[town].departures = 0;
                    towns[town].planes = [];
                    towns[town].planes.push(plane);
                } else {
                    towns[town].arrivals += people;
                    if (!checkPlanes(town, plane)) {
                        towns[town].planes.push(plane);
                    }
                }
            }
        } else if (action === 'depart') {
            if (availablePlanes[plane] !== undefined) {
                if (towns[town] === undefined) {
                    towns[town] = {};
                    towns[town].townName = town;
                    towns[town].arrivals = 0;
                    towns[town].departures = people;
                    towns[town].planes = [];
                    towns[town].planes.push(plane);
                } else {
                    towns[town].departures += people;
                    if (!checkPlanes(town, plane)) {
                        towns[town].planes.push(plane);
                    }
                }
                delete availablePlanes[plane];
            }
        }
    }

    for (let line in towns) {
        dataArr.push(towns[line]);
    }

    for (let plane in availablePlanes) {
        sortAvailablePlanes.push(plane);
    }

    sortAvailablePlanes.sort((a, b) => a.toLowerCase() > b.toLowerCase());
    dataArr.sort((a, b) => b.arrivals - a.arrivals || a.townName.toLowerCase() < b.townName.toLowerCase());

    // PRINT RESULT
    // print planes left
    console.log('Planes left:');
    for (let plane of sortAvailablePlanes) {
        console.log(`- ${plane}`);
    }

    for (let obj of dataArr) {
        let planesSort = obj.planes.sort((a, b) => a.toLowerCase() > b.toLowerCase());
        console.log(obj.townName);
        console.log(`Arrivals: ${obj.arrivals}`);
        console.log(`Departures: ${obj.departures}`);
        console.log('Planes:');
        for (let p of planesSort) {
            console.log(`-- ${p}`);
        }
    }
    // ==================================================================================

    function checkPlanes(town, id) {
        for (let plane of towns[town].planes) {
            if (id === plane) {
                return true;
            }
        }
        return false
    }
}

//solve([ "Airbus Paris 356 land", "Airbus London 321 land", "Airbus Paris 213 depart", "Airbus Ljubljana 250 land"])
//solve([ "Boeing747 Madrid 300 land", "AirForceOne WashingtonDC 178 land", "Boeing747 Sofia 300 depart", "Airbus London 265 depart", "ATR72 WashingtonDC 272 land", "ATR72 Madrid 135 depart"])
solve([ "Boeing474 Madrid 300 land", "AirForceOne WashingtonDC 178 land", "Airbus London 265 depart", "ATR72 WashingtonDC 272 land", "ATR72 Madrid 135 depart"]);