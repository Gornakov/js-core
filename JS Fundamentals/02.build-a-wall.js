function buildWall(input) {
    let data = input.map(Number);
    let concrete = 195;
    let cost = 1900;
    let concreteArr = [];
    let count = data.length;
    let total = 0;

    while(count > 0) {
        for (let i = 0; i < data.length; i++) {
            if (data[i] === 30) {
                count--;
            }
            data[i]++;
        }
        if (count > 0) {
            concreteArr.push(concrete * count);
        }
    }

    for (let line of concreteArr) {
        total += line;
    }
    total *= cost;

    console.log(concreteArr.join(', '));
    console.log(`${total} pesos`);
}

buildWall([21, 25, 28])
buildWall([17])
buildWall([17, 22, 17, 19, 17])