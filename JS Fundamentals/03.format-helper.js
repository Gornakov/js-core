function formatHelper([input]){
    let addSpaceAfterSigns = /([.,!?:;])(\s*)/g;
    let removeSpaceBeforeSigns = /(\s+)([.,!?:;])/g;
    let printDateString = /(\.)\s+(\d+)/g;
    let textInQuotes = /"([^"]+)"/g;

    let result = input
        .replace(addSpaceAfterSigns, (x) => x.trim() + ' ')
        .replace(removeSpaceBeforeSigns, (x) => x.trim())
        .replace(printDateString, (x) => x.split(' ').filter( x => x != '').join(''))
        .replace(textInQuotes, (x) => `"${x.substr(1, x.length - 2).trim()}"`);

    console.log(result);
}

formatHelper(['Terribly formatted text . With chaotic spacings.     " Terrible quoting "! Also this date is pretty confusing : 20 . 12. 16 .'])
formatHelper(['Make,sure to give:proper spacing where is needed .. . !'])