function solve(input) {
    let planesAvailable = {};
    let planesLeft = [];
    let sortedPlanesAvailable;
    let towns = {};
    let data = new Array();
    let sortedByTwoCriteria;

    for (let line of input) {
        let elements = line.split(' ');

        let planeId = elements[0];
        let town = elements[1];
        let passCount = Number(elements[2]);
        let action = elements[3];

        if (action === 'land') {
            if (planesAvailable[planeId] === undefined) {
                planesAvailable[planeId] = [];

                if (towns[town] === undefined) {
                    towns[town] = {};
                    towns[town]['townName'] = town;
                    towns[town]['arrivals'] = passCount;
                    towns[town]['departures'] = 0;
                    towns[town]['planes'] = [];
                    towns[town]['planes'].push(planeId);

                } else {
                    towns[town]['arrivals'] += passCount;
                    for (let plane of towns[town]['planes']) {
                        if (plane === planeId) {
                            return;
                        }
                    }
                    towns[town]['planes'].push(planeId);
                }
            }
        }
        else if (action === 'depart') {
            if (planesAvailable[planeId] !== undefined) {
                towns[town]['planes'].push(planeId);
                towns[town]['departures'] += passCount;
                delete planesAvailable[planeId];
            }
        }
    }

    // get all available planes
    for (let plane of Object.keys(planesAvailable)) {
        planesLeft.push(plane);
    }
    sortedPlanesAvailable = planesLeft.sort((a, b) => a.toLowerCase() > b.toLowerCase());

    for (let obj in towns) {
        data.push(towns[obj]);
    }

    // sort result
    sortedByTwoCriteria = data.sort((a, b) => b.arrivals - a.arrivals || a.townName.toLowerCase() > b.townName.toLowerCase());

    // print the RESULT

    // print planes left
    console.log('Planes left:');
    for (let plane of sortedPlanesAvailable) {
        console.log(`- ${plane}`);
    }

    for (let obj of sortedByTwoCriteria) {
        let planesSort = obj.planes.sort((a, b) => a.toLowerCase() > b.toLowerCase());
        console.log(obj.townName);
        console.log(`Arrivals: ${obj.arrivals}`);
        console.log(`Departures: ${obj.departures}`);
        console.log('Planes:');
        for (let i = 0; i < planesSort.length; i++) {
            let current = planesSort[i];
            if (current !== planesSort[i - 1] ) {
                console.log(`-- ${planesSort[i]}`);
            }
        }
    }
}

//solve([ "Airbus Paris 356 land", "Airbus London 321 land", "Airbus Paris 213 depart", "Airbus Ljubljana 250 land"])
//solve([ "Boeing474 Madrid 300 land", "AirForceOne WashingtonDC 178 land", "Airbus London 265 depart", "ATR72 WashingtonDC 272 land", "ATR72 Madrid 135 depart"])
