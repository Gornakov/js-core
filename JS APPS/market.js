function startApp() {
    showHideMenuLinks();
    if (sessionStorage.getItem('authToken')) {
        showView('viewUserHome')
    } else {
        showView('viewAppHome')
    }

    // hide all notifications
    $("#loadingBox").css("display", "none");
    $('#infoBox').css("display", "none");
    $('#errorBox').css("display", "none");

    // bind navigation links
    $("#linkMenuAppHome").click(showHomeView);
    $("#linkMenuLogin").click(showLoginView);
    $("#linkMenuRegister").click(showRegisterView);

    $("#linkMenuUserHome").click(showUserHomeView);
    $("#linkMenuShop").click(showShopView);
    $("#linkMenuCart").click(showCartView);
    $("#linkMenuLogout").click(logoutUser);

    $("#linkUserHomeShop").click(showShopView);
    $("#linkUserHomeCart").click(showCartView);

    // bind forms buttons
    $("#formLogin").submit(loginUser);
    $("#formRegister").submit(registerUser);

    // set kinvey data
    const baseKinveyUrl = "https://baas.kinvey.com/";
    const appId = "kid_H1DSgMtEx";
    const appSecret = "da0f02ee8752428d9a77be7351740576";

    // get authorization data
    function getAuthData(auth) {
        let header = { "Authorization" : ''};

        switch (auth) {
            case "basic":
                header['Authorization']="Basic " + btoa(appId + ":" + appSecret);
                break;
            case "kinvey":
                header['Authorization']="Kinvey " + sessionStorage.getItem('authToken');
                break;
            default: break;
        }

        return header;
    }

    // UNREGISTERED Users
    function showHomeView() {
        showView('viewAppHome');
    }

    function showLoginView() {
        $('#formLogin').trigger('reset');
        showView('viewLogin');
    }

    function showRegisterView() {
        $('#formRegister').trigger('reset');
        showView('viewRegister');
    }

    // REGISTERED Users
    function showUserHomeView() {
        showView('viewUserHome');
    }

    function showShopView() {
        showView('viewShop');
        let getProductsRequest = {
            method: "GET",
            url: baseKinveyUrl + "appdata/" + appId + "/products",
            headers: getAuthData('kinvey')
        };

        $.ajax(getProductsRequest)
            .then(loadProductsSuccess)
            .catch(handleAjaxError);

        function loadProductsSuccess(products) {
            let tableBody = $('#shopProducts').find('tbody');
            tableBody.empty();

            if (products.length !== 0 && products.length !== undefined) {
                for (let product of products) {
                    let prodPrice = (Math.round(product.price * 100) / 100).toFixed(2);
                    tableBody.append($('<tr>').append(
                        $('<td>').text(product.name),
                        $('<td>').text(product.description),
                        $('<td>').text(prodPrice),
                        $('<td>').append($('<button>Purchase</button>')
                            .click(purchaseProduct.bind(this, product)))
                    ));
                }
            }
        }
    }

    function showCartView() {
        showView('viewCart');
        let userId = sessionStorage.getItem('userId');
        let getCartRequest = {
            method: "GET",
            url: baseKinveyUrl + "user/" + appId + "/" + userId,
            headers: getAuthData('kinvey')
        };

        $.ajax(getCartRequest)
            .then(loadCartSuccess)
            .catch(handleAjaxError);

        function loadCartSuccess(items) {
            let tableBody = $('#cartProducts').find('tbody');
            tableBody.empty();

            if (items.cart !== undefined) {
                for (let item in items.cart) {
                    let itemData = items.cart[item];
                    let totalPrice = (Math.round(itemData.quantity * itemData.product.price * 100) / 100).toFixed(2);
                    tableBody.append($('<tr>').append(
                        $('<td>').text(itemData.product.name),
                        $('<td>').text(itemData.product.description),
                        $('<td>').text(itemData.quantity),
                        $('<td>').text(totalPrice),
                        $('<td>').append($('<button>Discard</button>')
                            .click(removeItem.bind(this, itemData)))
                    ));
                }
            }
        }
    }

    // ACTIONS
    // Purchase product
    function purchaseProduct(data) {
        let userId = sessionStorage.getItem('userId');
        let itemId = data._id;
        let getCartRequest = {
            method: "GET",
            url: baseKinveyUrl + "user/" + appId + "/" + userId,
            headers: getAuthData('kinvey')
        };

        $.ajax(getCartRequest)
            .then(loadCardDataSuccess)
            .catch(handleAjaxError);

        function loadCardDataSuccess(cardData) {
            let newCard = {
                quantity: 1,
                product: {
                    description: data.description,
                    name: data.name,
                    price: data.price
                }
            };
            if (cardData.cart[itemId] !== undefined) {
                newCard.quantity = cardData.cart[itemId].quantity + 1;
            } else {
                newCard[itemId] = {
                    quantity: 1,
                    product: {
                        description: data.description,
                        name: data.name,
                        price: data.price
                    }
                }
            }

            let card =
            updateInfo(newCard);
        }

    }
    // Update Info
    function updateInfo(info) {
        let userId = sessionStorage.getItem('userId');
        let userData = {
            username: sessionStorage.getItem('username'),
            name: sessionStorage.getItem('fullname'),
            cart: info
        };

        let updateRequest = {
            method: "PUT",
            url: baseKinveyUrl + "user/" + appId + "/" + userId,
            headers: getAuthData('kinvey'),
            data: cart
        };


        $.ajax(updateRequest)
            .then(loadUpdateSuccess)
            .catch(handleAjaxError);

        function loadUpdateSuccess(data) {
            showCartView();
            showInfo('Product purchased.');
        }
    }

    // Remove Item
    function removeItem(data) {
        let userId = sessionStorage.getItem('userId');
        let userUsername = sessionStorage.getItem('username');
        let userFullName = sessionStorage.getItem('fullname');



        let userData = {
            username: sessionStorage.getItem('username'),
            name: sessionStorage.getItem('fullname'),
            cart: cartData
        };

        let removeRequest = {
            method: "PUT",
            url: baseKinveyUrl + "user/" + appId + "/" + userId,
            headers: getAuthData('kinvey'),
            data: userData
        };

        $.ajax(removeRequest)
            .then(removeSuccess)
            .catch(handleAjaxError);

        function removeSuccess(response) {
            showCartView();
            showInfo('Product discarded.');
        }
    }

    //REGISTER
    function registerUser() {
        let userFullName = $('#formRegister input[name=name]').val();
        let userData = {
            username: $('#formRegister input[name=username]').val(),
            password: $('#formRegister input[name=password]').val(),
            name: userFullName
        };

        let registerRequest = {
            method: "POST",
            url: baseKinveyUrl + "user/" + appId,
            headers: getAuthData('basic'),
            data: userData
        };

        $.ajax(registerRequest)
            .then(registerSuccess)
            .catch(handleAjaxError);

        function registerSuccess(userInfo) {
            saveAuthInSession(userInfo);
            showHideMenuLinks();
            showUserHomeView();
            showInfo('User registration successful.');
        }
    }

    //LOGIN
    function loginUser() {
        let userData = {
            username: $('#formLogin input[name=username]').val(),
            password: $('#formLogin input[name=password]').val()
        };

        let loginRequest = {
            method: "POST",
            url: baseKinveyUrl + "user/" + appId + "/login",
            headers: getAuthData('basic'),
            data: userData
        };

        $.ajax(loginRequest)
            .then(loginSuccess)
            .catch(handleAjaxError);

        function loginSuccess(userInfo) {
            saveAuthInSession(userInfo);
            showHideMenuLinks();
            showUserHomeView();
            showInfo('Login successful.');
        }
    }

    // LOGOUT
    function logoutUser() {
        let logoutRequest = {
            method: "POST",
            url: baseKinveyUrl + "user/" + appId + "/_logout",
            headers: getAuthData('kinvey')
        };

        $.ajax(logoutRequest)
            .then(logoutSuccess)
            .catch(handleAjaxError);

        function logoutSuccess() {
            sessionStorage.clear();
            showHideMenuLinks();
            showInfo('Logout successful.');
            showView('viewAppHome');
            $('#spanMenuLoggedInUser').text("");
            $('#viewUserHomeHeading').text("");
        }
    }

    // Attach AJAX "loading" event listener
    $(document).on({
        ajaxStart: function() { $("#loadingBox").show() },
        ajaxStop: function() { $("#loadingBox").hide() }
    });

    // prevent default action of submits
    $("form").submit(function(e) {
        e.preventDefault();
    });


    // Bind the info / error boxes: hide on click
    $("#infoBox, #errorBox").click(function() {
        $(this).fadeOut();
    });

    // display chosen view
    function showView(viewName) {
        // Hide all views and show the selected view only
        $('main > section').css("display", "none");
        $('#' + viewName).removeAttr("style");
    }
    
    function showHideMenuLinks() {
        if (sessionStorage.getItem('authToken')) {
            // We have logged in user
            $("#linkMenuUserHome").removeAttr("style");
            $(".anonymous").css("display", "none");
            $(".useronly").removeAttr("style");

            $('#spanMenuLoggedInUser').text(
                "Welcome, " + sessionStorage.getItem('username') + "!");
            $('#viewUserHomeHeading').text(
                "Welcome, " + sessionStorage.getItem('username') + "!");
        } else {
            // No logged in user
            $("#linkMenuAppHome").removeAttr("style");
            $(".anonymous").removeAttr("style");
            $(".useronly").css("display", "none");
        }
    }

    // set session storage
    function saveAuthInSession(userInfo) {
        let userAuth = userInfo._kmd.authtoken;
        sessionStorage.setItem('authToken', userAuth);
        let userId = userInfo._id;
        sessionStorage.setItem('userId', userId);
        let username = userInfo.username;
        sessionStorage.setItem('username', username);
        let fullname = userInfo.name;
        if (fullname === undefined) {
            fullname = '';
        }
        sessionStorage.setItem('fullname', fullname);
    }


    // set error info
    function handleAjaxError(response) {
        let errorMsg = JSON.stringify(response);
        if (response.readyState === 0)
            errorMsg = "Cannot connect due to network error.";
        if (response.responseJSON &&
            response.responseJSON.description)
            errorMsg = response.responseJSON.description;
        showError(errorMsg);
    }

    // set notifications
    function showInfo(message) {
        $('#infoBox').text(message);
        $('#infoBox').show();
        setTimeout(function() {
            $('#infoBox').fadeOut();
        }, 3000);
    }

    function showError(errorMsg) {
        $('#errorBox').text("Error: " + errorMsg);
        $('#errorBox').show();
    }
}