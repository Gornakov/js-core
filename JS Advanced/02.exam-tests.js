let makeList = require('./02.unit-testing');
let expect = require('chai').expect;

// ====================================================================

describe("Test cases for list functionality", function () {
    let myList = {};

    beforeEach(function () {
        myList = makeList();
    });

    it("shoud have some properties", function () {
        expect(myList.addLeft).to.exists;
        expect(myList.addRight).to.exists;
        expect(myList.clear).to.exists;
        expect(myList.toString).to.exists;
    });

    it('should be empty on start', function () {
        expect(myList.toString()).to.equal('');
    });

    it('shoud add on the left', function () {
        myList.addLeft('hello left');
        expect(myList.toString()).to.equal('hello left');
    });

    it('shoud add on the right', function () {
        myList.addRight('hello right');
        expect(myList.toString()).to.equal('hello right');
    });

    it('should addLeft multiple items', function () {
        myList.addLeft('add left');
        myList.addLeft('hello left');
        expect(myList.toString()).to.equal('hello left, add left');
    });

    it('should addLeft multiple items', function () {
        myList.addRight('add right');
        myList.addRight('hello right');
        expect(myList.toString()).to.equal('add right, hello right');
    });

    it('should set empty object on myList.clear', function () {
        myList.addRight('hello right');
        myList.addLeft('hello left');
        myList.clear();
        expect(myList.toString()).to.equal('');
    });
});