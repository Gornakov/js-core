let makeList = require('./02.unit-testing');
let expect = require('chai').expect;

describe("Test cases for list functionality", function () {
    let myList = {};

    beforeEach(function () {
        myList = makeList();
    });

    it("list should have some properties", function () {
        expect(typeof myList).to.equal('object');

        expect(typeof myList.addLeft).to.equal('function');
        expect(typeof myList.addRight).to.equal('function');
        expect(typeof myList.clear).to.equal('function');
        expect(typeof myList.toString).to.equal('function');
    });

    it('check properties types', () => {
        expect(myList.hasOwnProperty('addLeft')).to.equal(true);
        expect(myList.hasOwnProperty('addRight')).to.equal(true);
        expect(myList.hasOwnProperty('clear')).to.equal(true);
        expect(myList.hasOwnProperty('toString')).to.equal(true);
    });

    it('should be empty on init', () => {
        expect(myList.toString()).to.equal('');
    });

    it('should add element on addRight(item)', () => {
        myList.addRight(5);
        expect(myList.toString()).to.equal('5');
    });

    it('should add element on addLeft(item)', () => {
        myList.addLeft(4);
        expect(myList.toString()).to.equal('4');
    });

    it('should add elements of any type on addRight(item)', () => {

        myList.addLeft(5);
        myList.addLeft("six");
        myList.addRight(false);
        expect(myList.toString()).to.equal('six, 5, false');
    });

    it('should work correctly on this complex test', () => {
        let items = ['asd', 1, 15, 888, 14, {name: 'pesho'}, false, true, NaN, 0];
        for (let item of items) {
            myList.addRight(item);
        }

        expect(myList.toString()).to.equal('asd, 1, 15, 888, 14, [object Object], false, true, NaN, 0');
    });

    it('should empty the array', () => {
        let items = ['asd', 1, 15, 888, 14, {name: 'pesho'}, false, true, NaN, 0];
        for (let item of items) {
            myList.addRight(item);
        }
        myList.clear();

        expect(myList.toString()).to.equal('');
    });
});