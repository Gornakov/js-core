function move(direction) {
    let selected = $('#towns option:selected');

    if (direction === -1) {
        //moveUp();
        selected.insertBefore(selected.prev());
    } else if (direction === +1) {
        //moveDown();
        selected.insertAfter(selected.next());
    }
    /*
    function moveUp() {
        let prevElement = selected.prev();
        if (prevElement.length > 0) {
            selected.detach().insertBefore(prevElement);
        }
    }
    
    function moveDown() {
        let nextElement = selected.next();
        if (nextElement.length > 0) {
            selected.detach().insertAfter(nextElement);
        }
    }
    */
}