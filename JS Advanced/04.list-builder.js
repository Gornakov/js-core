function listBuilder(selector) {
    /*
    let result = $(selector);

    function createNewList() {
        result.empty();
        result.append('<ul>');
    }

    function addItem(text) {
        let btnUp = $('<button>').text('Up').on('click', function () {
            let li = $(this).parent();

            if (li.is(':first-child'))
                ul.append(li);
            else
                li.insertBefore(li.prev());
        });
        let btnDown = $('<button>').text('Down').on('click', function () {
            let li = $(this).parent();

            if (li.is(':last-child'))
                ul.prepend(li);
            else
                li.insertAfter(li.next());
        });
        result.find('ul')
            .append($('<li>').text(text).append(btnUp).append(btnDown));
    }

    return {
        createNewList,
        addItem
    };
    */
    return {
        createNewList: function() {
            let ul = $("<ul>");
            $(selector).empty();
            $(selector).append(ul);
        },
        addItem: function(text) {
            let li = $("<li>").text(text);
            li.append($("<button>Up</button>").click(this.buttonUp));
            li.append($("<button>Down</button>").click(this.buttonDown));
            $(selector + " ul").append(li);
        },
        buttonUp: function() {
            let li = $(this).parent();
            li.insertBefore(li.prev());
        },
        buttonDown: function() {
            let li = $(this).parent();
            li.insertAfter(li.next());
        }
    }
}